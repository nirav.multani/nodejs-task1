const express = require('express')
const usersMod = require('./utils/users')

const app = express()


const publicPath = '/home/kevit/nirav/node dir/task/public'
app.use(express.static(publicPath))


//app.com
app.get('',(req, res) => {
    
})
//app.com/users

//app.com/users/create
app.get('/users/createProcess.html', (req, res) => {

    usersMod.createUser(req.query.name,req.query.email ,(result) => {
        res.send(result)
    })
})

//app.com/users/find
app.get('/users/findProcess.html', (req, res) => {

    usersMod.getOneUser(req.query.name, (result) => {
        res.send(result)
    })
})

//app.com/users/show all user
app.get('/users/findAllProcess.html', (req, res) => {

    usersMod.getAllUser((result) => {
        res.send(result)
    })
})

//app.com/users/updateUser
app.get('/users/updateUserProcess.html', (req, res) => {

    usersMod.updateUser(req.query.oldname, req.query.newname, req.query.newemail, (result) => {
        res.send(result)
    })
})

//app.com/users/deleteUser
app.get('/users/deleteUserProcess.html', (req,res) => {

    usersMod.deleteUser(req.query.name, (result) => {
        res.send(result)
    })
})
//app.com/album
//app.com/photos

app.listen(3000, ()=>{
    console.log( 'server started on port no 3000' )
})