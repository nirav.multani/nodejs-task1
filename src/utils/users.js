// User: - Create 
// - Get all
// - Get one
// - Update one 
// - Delete one
// - List all the photographs by the users.
// - List all the albums that contains photographs tagged for these users. 

const mongoose = require('mongoose')

//connetct to user collection
mongoose.connect('mongodb://127.0.0.1:27017/users', {
    useCreateIndex: true,
    useNewUrlParser : true
})


//================models================
//create user model 
const User = mongoose.model('User', {
    name:{
        type: String,
        //required: true
    },
    email: {
        type: String
    }
})

//============methods=========================
//create user
const createUser = (name, email, callback) => {

    //add user to db through model
    const createUser = new User({
        name,
        email
    })
    //upload to db
    createUser.save().then( (result)=> {
        //callback('data entered')
        callback(result)
    }).catch( (error)=> {
        callback('error occured during inserting data')
        //callback(error)
    })
}

//get all user from db--------------------------

const getAllUser = (callback) =>{
    User.find({}).then((result)=>{
        callback(result)
    }).catch((error) => {
        console.error(error)
    })

}

//get one user-----------------------------

const getOneUser = (uname, callback) => {
    User.find({
        name : uname
    }).then((result) => {
        callback(result)
    }).catch( (error) => {
        console.error(error)
    })
}

//update one ---------------------------
const updateUser = (oldUserName, newUserName, newEmail, callback) => {
    User.updateOne({
        name : oldUserName
    },{
        name: newUserName,
        email: newEmail
    }).then( (result) => {
        callback(result)
    } ).catch( (error) => {
        callback(error)
    })
}


//delete one---------------------------------

const deleteUser = (uname, callback) => {
    User.deleteOne({
        name: uname
    }).then((result) => {
        callback(result)
    }).catch((error) => {
        callback(error)
    })
}

//list of pic by user


//list of pic where user is tagged


//export all the methods 

module.exports = {
    createUser,
    getOneUser,
    getAllUser,
    updateUser,
    deleteUser
}