const findUserForm = document.querySelector('form')
const msgField = document.querySelector('#msgField')
const findButton = document.querySelector('#findButton')

findUserForm.addEventListener('submit', (e) => {
    e.preventDefault()
  

    console.log('connected to client side java script')

    fetch('/users/findAllProcess.html').then((response) => {
       console.log('fetch is working')   
        response.json().then((data) => {
        console.log(data)

            data.forEach(element => {
                msgField.innerHTML += `<div class="row"> 
                                            <div class="col">`+ element._id +` </div>
                                            <div class="col">`+ element.name +` </div>
                                            <div class="col">`+ element.email +` </div>
                                        </div>`
            })

       

        findButton.classList.add('disabled')
    })
    
        
    }).catch((error) => {
        msgField.textContent = 'error occured'
       // console.log(error)
    }) 

    
    
})