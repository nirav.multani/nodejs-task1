const deleteUserForm = document.querySelector('form')
const nameField = document.querySelector('#nameField')
const msgField = document.querySelector('#msgField')



deleteUserForm.addEventListener('submit', (e) => {
    e.preventDefault()

    //take input from from 
    const name = nameField.value

    console.log('connected to client side java script')


    //fetch the result from processing db methods
    fetch('/users/deleteUserProcess.html?name='+name).then((response) => {
      //  console.log('fetch is working')

        response.json().then((data) => {
    
            checkFlag = data.deletedCount
            if(checkFlag === 0){
                msgField.textContent = 'error deleting data.. , user may not exits'
            } 
            else{
                msgField.textContent = 'success! user deleted'
            }
        })
        
    }).catch((error) => {
        msgField.textContent = 'error occured'
       // console.log(error)
    }) 

    nameField.value = ''
})