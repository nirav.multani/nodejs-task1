const findUserForm = document.querySelector('form')
const nameField = document.querySelector('#nameField')
const msgField1 = document.querySelector('#msgField1')
const msgField2 = document.querySelector('#msgField2')
const msgField3 = document.querySelector('#msgField3')



findUserForm.addEventListener('submit', (e) => {
    e.preventDefault()

    const name = nameField.value

    console.log('connected to client side java script')

    fetch('/users/findProcess.html?name='+name).then((response) => {
       console.log('fetch is working')
       
            
        response.json().then((data) => {
            console.log(data)

            if(data[0] === undefined){
                 msgField1.textContent = 'NO NAME FOUND'
            }
            else{
                msgField1.textContent = 'NAME : '+data[0].name
                msgField2.textContent = 'EMAIL : '+data[0].email
                msgField3.textContent = 'ID : '+data[0]._id

            }

        })
        
        
    }).catch((error) => {
        msgField.textContent = 'error occured'
       // console.log(error)
    }) 

    nameField.value = ''
    
})