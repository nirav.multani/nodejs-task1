const createUserForm = document.querySelector('form')
const nameField = document.querySelector('#nameField')
const emailField = document.querySelector('#emailField')
const msgField = document.querySelector('#msgField')



createUserForm.addEventListener('submit', (e) => {
    e.preventDefault()

    //take input from from 
    const name = nameField.value
    const email = emailField.value

    console.log('connected to client side java script')


    //fetch the result from processing db methods
    fetch('/users/createProcess.html?name='+name+'&email='+email).then((response) => {
      //  console.log('fetch is working')

        response.json().then((data) => {

            msgField.innerHTML = `data stored <br>
                                ID:` +data._id + `<br>
                                NAME:` +data.name+ `<br>
                                EMAIL: `+data.email

        })
        
    }).catch((error) => {
        msgField.textContent = 'error occured'
       // console.log(error)
    }) 

    nameField.value = ''
    emailField.value = ''
})