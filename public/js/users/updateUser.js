const createUserForm = document.querySelector('form')
const oldnameField = document.querySelector('#oldnameField')
const newnameField = document.querySelector('#newnameField')
const newemailField = document.querySelector('#newemailField')
const msgField = document.querySelector('#msgField')



createUserForm.addEventListener('submit', (e) => {
    e.preventDefault()

    //take input from from 
    const oldname = oldnameField.value
    const newname = newnameField.value
    const newemail = newemailField.value

    //fetch the result from processing db methods
    fetch('/users/updateUserProcess.html?oldname='+oldname+'&newname='+newname+'&newemail='+newemail).then((response) => {


        response.json().then((data) => {
            console.log(data)
            
            const modified = data.nModified
            if(modified === 1) {
                msgField.textContent = 'user value updated'
            }
            else{
                msgField.textContent = 'error : not updated, user may not exist '
            }
        })
        
    }).catch((error) => {
        msgField.textContent = 'error occured'
       // console.log(error)
    }) 

    oldnameField.value = ''
    newnameField.value = ''
    newemailField.value = ''
})